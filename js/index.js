$('#mix-wrapper').mixItUp({
    load: {
        sort: 'order:asc'
    },
    animation: {
        effects: 'fade scale(0.95)',
        duration: 400
    },
    selectors: {
        target: '.mix-target',
        filter: '.filter-btn',
        sort: '.sort-btn'
    },
    callbacks: {
        onMixEnd: function(state) {
            console.log(state)
        }
    }
});
