$('.detailSlider').slick(

    {
        asNavFor: '.detailThumbs',
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        fade: true,
        arrows: false,
        infinite: false,
        responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                fade: false
            }
        }]
    }
);

$('.detailThumbs').slick(

    {
        asNavFor: '.detailSlider',
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        vertical: true,
        focusOnSelect: true,
        infinite: false,

    }
);